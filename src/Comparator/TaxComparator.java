package Comparator;

import java.util.Comparator;

import Interface.Taxable;

public class TaxComparator implements Comparator<Taxable>{

	@Override
	public int compare(Taxable firstObj, Taxable secondObj) {
		double firstTax = firstObj.getTax();
		double secondTax = secondObj.getTax();
		if(firstTax > secondTax)
			return 1;
		else if(firstTax < secondTax)
			return -1;
		else 
			return 0;
	}

}
