package Comparator;

import java.util.Comparator;

import Model.Company;

public class ExpenseComparator implements Comparator<Company>{

	@Override
	public int compare(Company company1, Company company2) {
		if(company1.getExpenses() < company2.getExpenses())
			return -1;
		else if(company1.getExpenses() > company2.getExpenses())
			return 1;
		else 
			return 0;
	}

}
