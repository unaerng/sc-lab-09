package Comparator;
import java.util.Comparator;

import Model.Company;


public class EarningComparator implements Comparator<Company>{

	@Override
	public int compare(Company company1, Company company2) {
		if(company1.getReceive() < company2.getReceive())
			return -1;
		else if(company1.getReceive()> company2.getReceive())
			return 1;
		else 
			return 0;
	}

}
