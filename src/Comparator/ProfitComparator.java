package Comparator;

import java.util.Comparator;

import Model.Company;

public class ProfitComparator implements Comparator<Company>{

	@Override
	public int compare(Company company1, Company company2) {
		if(company1.getProfits() < company2.getProfits())
			return -1;
		else if(company1.getProfits() > company2.getProfits())
			return 1;
		else 
			return 0;
		
	}

}
