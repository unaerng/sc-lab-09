package Model;

import Interface.Taxable;

public class Company implements Taxable {
	private String name;
	private double receive;
	private double expenses;

	public Company(String name, double receive, double expenses) {
		this.name = name;
		this.receive = receive;
		this.expenses = expenses;
	}

	public String getName() {
		return this.name;
	}

	public double getReceive() {
		return this.receive;
	}

	public double getProfits() {
		double sum = 0;
		sum = receive - expenses;
		return sum;
	}

	public double getExpenses() {
		return this.expenses;
	}

	@Override
	public double getTax() {
		double sum = 0;
		sum = (this.getReceive() - this.getExpenses())*0.30;
		return sum;
	}

	public String toString() {
		return this.getName() + " earnings: " + this.getReceive() + " expenses: "
				+ this.getExpenses() + " profits: " + this.getProfits();
	}

}
