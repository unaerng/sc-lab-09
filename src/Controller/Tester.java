package Controller;

import java.util.ArrayList;
import java.util.Collections;

import Comparator.EarningComparator;
import Comparator.ExpenseComparator;
import Comparator.ProfitComparator;
import Comparator.TaxComparator;
import Interface.Taxable;
import Model.Company;
import Model.Person;
import Model.Product;

public class Tester {

	public static void main(String[] args) {
		
		Tester tester = new Tester();
			tester.testPerson();
			tester.testProduct();
			tester.testCompanies();
//			tester.testTax();
	}
	
	private void testPerson(){
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Harry", 400000));
		persons.add(new Person("John", 200000));
		persons.add(new Person("Jame", 62000));
		Collections.sort(persons);
		for(Person p:persons){
			System.out.println(p);
		}
		System.out.println("----------------------------------------------");
	}
	
	private void testProduct(){
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("Table", 2000));
		products.add(new Product("Notebook", 35000));
		products.add(new Product("Phone", 25000));
		Collections.sort(products);
		for(Product p:products){
			System.out.println(p);
		}
		System.out.println("----------------------------------------------");
	}
	
	private void testCompanies(){
		ArrayList<Company> companies = new ArrayList<Company>();
		companies.add(new Company("Nokie", 3000000, 2000000));
		companies.add(new Company("Sumsung", 1000000, 500000));
		companies.add(new Company("Apple", 8000000, 500000));

		Collections.sort(companies,new EarningComparator());
		for(Company c : companies){
			System.out.println(c);
		}
		System.out.println("----------------------------------------------");
	
		Collections.sort(companies,new ExpenseComparator());
		for(Company c : companies){
			System.out.println(c);
		}
		System.out.println("----------------------------------------------");
		
		Collections.sort(companies,new ProfitComparator());
		for(Company c : companies){
			System.out.println(c.getProfits());
		}
		
		System.out.println("----------------------------------------------");

	}	
}